package com.company;

public class Human {
    private String firstName;
    private String secondName;
    private String patronymic;

    public Human(String firstName, String secondName) {
        this(firstName, secondName, null);
    }

    public Human(String firstName, String secondName, String patronymic) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.patronymic = patronymic;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getFullName() {
        String fullName;
        if (patronymic == null) {
            fullName = this.secondName + " " + this.firstName;
        } else {
            fullName = this.secondName + " " + this.firstName + " " + this.patronymic;
        }
       return fullName;
    }

    public String getShortName() {
        String shortName;
        if (patronymic == null) {
            shortName = this.secondName + " " + this.firstName.charAt(0) + ".";
        } else {
            shortName = this.secondName + " " + this.firstName.charAt(0) + ". " + this.patronymic.charAt(0) + ".";
        }
        return shortName;
    }
}
