package com.company;

public class Main {

    public static void main(String[] args) {
        Human h1 = new Human("Иван", "Пупкин", "Васильевич");
        Human h2 = new Human("Иван","Пупкин");
        System.out.println(h1.getFullName());
        System.out.println(h1.getShortName());
        System.out.println(h2.getFullName());
        System.out.println(h2.getShortName());
    }
}
